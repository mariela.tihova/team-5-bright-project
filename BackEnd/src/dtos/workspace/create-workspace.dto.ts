import { Country } from "src/models/country.entity";

export class CreateWorkspaceDTO {


    // country: Country;


    numberOfDesks: number;


    matrix: string;
    nextWeekPlanningMatrix: string;


    width: number;


    height: number;

};
